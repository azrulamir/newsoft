<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'list_name', 'address', 'latitude', 'longtitude', 'submitter_id'
    ];

    /** Get the owner */
    public function user()
    {
        return $this->belongsTo('App\User', 'submitter_id', 'id');
    }
}
