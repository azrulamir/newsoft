<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Listing;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->latitude && $request->longtitude) {

            $listings = Listing::where('submitter_id', '=', $id)->get();
            $query_listings = [];

            foreach ($listings as $listing) {

                $pointA = [
                    "lat" => $request->latitude,
                    "long" => $request->longtitude
                ];

                $pointB = [
                    "lat" => $listing->latitude,
                    "long" => $listing->longtitude
                ];

                $listing->distance = $this->calculateDistance($pointA, $pointB);

                array_push($query_listings, $listing);
            }

            return response($query_listings, 200);
        }
        else {
            return response('Missing parameters!', 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $listing = Listing::find($id);

        if ($request->all()) {
            foreach ($request->all() as $req => $value) {
                $listing->$req = $value;
            }

            $listing->save();

            return response('Listing updated', 200);
        }
        else {
            return response('Missing parameters!', 400);
        }
    }

    /**
     * Calculate distance
     *
     * @return float $distance
     */
    public function calculateDistance($pointA, $pointB)
    {
        $degrees = rad2deg(acos((sin(deg2rad($pointA['lat'])) * sin(deg2rad($pointB['lat']))) + (cos(deg2rad($pointA['lat'])) * cos(deg2rad($pointB['lat'])) * cos(deg2rad($pointA['long'] - $pointB['long'])))));

        $distance = $degrees * 111.13384;

        return round($distance, 3);
    }
}
