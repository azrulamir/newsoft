<?php

namespace App\Http\Controllers;

class MinionController extends Controller
{
    private $primes;

    /**
     * Init
     */
    public function __construct()
    {
        $this->prepPrimes();
    }

    /**
     * Prepare concatenated prime numbers
     *
     * @param Integer $id
     * @return \Illuminate\Http\Response
     */
    public function prepPrimes()
    {
        $concat = "";
        for ($i = 0; strlen($concat) < 10005; $i++) {
            if ($this->isPrime($i)) {
                $concat = $concat . $i;
            }
        }
        return $this->primes = $concat;
    }

    /**
     * Return new minion ID number
     *
     * @param Integer $id
     * @return \Illuminate\Http\Response
     */
    public function answer($id)
    {
        return response('<h2>' . substr($this->primes, $id, 5) . '</h2>');
    }

    /**
     * Check for prime number
     *
     * @param Integer $id
     * @return Boolean
     */
    public function isPrime($num)
    {
        if ($num == 1)
            return false;

        if ($num == 2)
            return true;

        if ($num % 2 == 0) {
            return false;
        }

        $ceil = ceil(sqrt($num));
        for ($i = 3; $i <= $ceil; $i = $i + 2) {
            if ($num % $i == 0)
                return false;
        }

        return true;
    }
}
