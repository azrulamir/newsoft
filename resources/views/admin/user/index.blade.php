@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">User List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        @foreach ($users as $user)
                        <a href="{{ route('admin.user.edit', ['id' => $user->id]) }}" class="list-group-item list-group-item-action">
                            @if ($user->id == Auth::id())
                                {{ $user->name }} (You)
                            @else
                                {{ $user->name }}
                            @endif
                            <small class="float-right">{{ $user->email }}</small>
                        </a>
                        @endforeach
                    </div>

                <a href="{{ route('admin.user.create') }}" class="btn btn-success btn-sm btn-block mt-3">Add New User</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
