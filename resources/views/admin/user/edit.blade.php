@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Edit Existing User</div>

                <div class="card-body">
                    <form action="{{ route('admin.user.update', $user->id) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                            <input name="name" type="text" class="form-control" placeholder="" value="{{ $user->name }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                            <input name="email" type="email" class="form-control" placeholder="" value="{{ $user->email }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Type</label>
                            <div class="col-sm-9">
                                <select name="type" class="custom-select">
                                    <option value="a" @if ($user->type == 'a') selected @endif>Admin</option>
                                    <option value="u" @if ($user->type == 'u') selected @endif>User</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                            <input name="password" type="password" class="form-control" placeholder="" value="" required>
                            </div>
                        </div>

                        <a href="{{ route('admin.user.destroy', $user->id) }}" class="btn btn-danger"
                            onclick="
                                event.preventDefault();
                                if (confirm('Sure wanna delete?')) {
                                    document.getElementById('destroy-form').submit();
                                    }
                                ">
                                Delete
                            </a>

                        <div class="float-right">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="{{ route('admin.user.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </form>

                    <form id="destroy-form" action="{{ route('admin.user.destroy', $user->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
