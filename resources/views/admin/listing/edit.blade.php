@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Edit Existing List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('admin.listing.update', $listing->id) }}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                            <input name="list_name" type="text" class="form-control" placeholder="" value="{{ $listing->list_name }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                            <input name="address" type="text" class="form-control" placeholder="" value="{{ $listing->address }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Latitude</label>
                            <div class="col-sm-9">
                            <input name="latitude" type="number" step="any" class="form-control" placeholder="" value="{{ $listing->latitude }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Longtitude</label>
                            <div class="col-sm-9">
                            <input name="longtitude" type="number" step="any" class="form-control" placeholder="" value="{{ $listing->longtitude }}" required>
                            </div>
                        </div>

                        <a href="{{ route('admin.listing.destroy', $listing->id) }}" class="btn btn-danger"
                            onclick="
                                event.preventDefault();
                                if (confirm('Sure wanna delete?')) {
                                    document.getElementById('destroy-form').submit();
                                    }
                                ">
                                Delete
                            </a>

                        <div class="float-right">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a href="{{ route('admin.listing.index') }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </form>

                    <form id="destroy-form" action="{{ route('admin.listing.destroy', $listing->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
