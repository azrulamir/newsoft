@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">Listings</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        @foreach ($listings as $listing)
                    <a href="{{ route('admin.listing.edit', ['id' => $listing->id]) }}" class="list-group-item list-group-item-action">
                        {{ $listing->list_name }}
                        <small class="float-right">
                            @if (Auth::id() == $listing->user->id)
                                You
                            @else
                                {{ $listing->user->name }}
                            @endif
                        </small>
                    </a>
                        @endforeach
                    </div>

                <a href="{{ route('admin.listing.create') }}" class="btn btn-success btn-sm btn-block mt-3">Add New Listing</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
