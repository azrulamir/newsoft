<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Superadmin',
                'email' => 'superadmin@example.com',
                'password' => bcrypt('secret'),
                'type' => 'a'
            ],
            [
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'password' => bcrypt('secret'),
                'type' => 'a'
            ],
            [
                'name' => 'User',
                'email' => 'user@example.com',
                'password' => bcrypt('secret'),
                'type' => 'u'
            ]
        ]);
    }
}
