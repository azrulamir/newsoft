<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OAuth2ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'user_id' => 2,
            'name' => 'NewSoft Listing Client',
            'secret' => 'QRQsiobtQQzLfo0KM5fi8THP8thLSJEw5jgfF1vv',
            'redirect' => 'http://newsoft.localhost/auth/callback',
            'personal_access_client' => 0,
            'password_client' => 0,
            'revoked' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
