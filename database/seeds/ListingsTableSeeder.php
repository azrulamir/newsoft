<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ListingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('listings')->insert([
            [
                'list_name' => 'Starbucks @ Mid Valley Megamall',
                'address' => 'Lingkaran Syed Putra, Mid Valley City',
                'latitude' => '3.117880',
                'longtitude' => '101.676749',
                'submitter_id' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'list_name' => 'Ticklish Ribs & Wiches',
                'address' => 'REXKL 80, Jalan Sultan, 50000 Kuala Lumpur',
                'latitude' => '3.144261',
                'longtitude' => '101.698535',
                'submitter_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'list_name' => 'The Coffee Bean & Tea Leaf',
                'address' => 'Lingkaran Syed Putra, Mid Valley City',
                'latitude' => '3.117254',
                'longtitude' => '101.677860',
                'submitter_id' => 3,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
