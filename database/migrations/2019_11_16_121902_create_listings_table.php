<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('list_name', 80);
            $table->string('address');
            $table->float('latitude');
            $table->float('longtitude');
            $table->bigInteger('submitter_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('listings', function (Blueprint $table) {
            $table->foreign('submitter_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
