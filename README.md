# NewSoft

Newsoft is created for the purpose of testing.

## Installation

Use the package manager php composer to install.

```bash
composer install
```

## Setup

```php
php artisan migrate --seed
php artisan passport:keys
```

## Credentials

### Dashboard
The migration command seeded TWO users by default, ONE admin and ONE normal user with their types defined accordingly in their respective records.

```
1) admin@example.com
2) user@example.com

Password : secret
```

### API
The migration also seed ONE oauth2 client record to quickstart the testing proceess. The seeded client only meant to utilize Authorization Code grant type with the following credentials details.

```bash
Grant Type: Authorization Code
Callback URI: /auth/callback
Auth URI: /oauth/authorize
Access Token URI: /oauth/token
Client ID: 1
Client Secret: QRQsiobtQQzLfo0KM5fi8THP8thLSJEw5jgfF1vv
```
#### Endpoints URI
```js
GET: /api/listing/{id} // {id} = user id
params: {
    latitude,
    longtitude
}

PATCH: /api/listing/{id}/update // {id} = listing id
body: {
    listing attributes
}
```

## Part C
I had implemented the answer for part C of the assessment into this app as well which resided at this URI /minion/{id}, just replace {id} with any number between 1-10000 and the response will contain the answer accordingly.

## Note
This is built specifically for newsoft testing purposes.

## Improvements
```
1) Proper input validation; currently depend on client side to do the work.
2) Proper Exception handling.
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
