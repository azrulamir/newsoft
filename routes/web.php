<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/admin')->middleware(['auth', 'is-admin'])->group(function () {
    Route::get('user/', 'UserController@index')->name('admin.user.index');
    Route::get('user/create', 'UserController@create')->name('admin.user.create');
    Route::get('user/{id}/edit', 'UserController@edit')->name('admin.user.edit');
    Route::post('user', 'UserController@store')->name('admin.user.store');
    Route::patch('user/{id}', 'UserController@update')->name('admin.user.update');
    Route::delete('user/{id}', 'UserController@destroy')->name('admin.user.destroy');

    Route::get('listing/', 'ListingController@index')->name('admin.listing.index');
    Route::get('listing/create', 'ListingController@create')->name('admin.listing.create');
    Route::get('listing/{id}/edit', 'ListingController@edit')->name('admin.listing.edit');
    Route::post('listing', 'ListingController@store')->name('admin.listing.store');
    Route::patch('listing/{id}', 'ListingController@update')->name('admin.listing.update');
    Route::delete('listing/{id}', 'ListingController@destroy')->name('admin.listing.destroy');
});

Route::get('/minion/{id}', 'MinionController@answer')->name('minion.answer');

Route::get('/unauthorized', function() {
    return view('unauthorized');
});
